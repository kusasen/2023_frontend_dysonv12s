$(function(){
  $(".count-number").data("countToOptions", {
    formatter: function(value, options) {
      return value
        .toFixed(options.decimals)
        .replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
    }
  });

  function count(options) {
    var $this = $(this);
    options = $.extend({}, options || {}, $this.data("countToOptions") || {});
    $this.countTo(options);
  }

  // images loaded
  var imgLoad = imagesLoaded('#wrapper');

  var progressBar = $(".load-bar"),
      images = $("img").length,
      loadedCount = 0,
      loadingProgress = 0,
      tlProgress = new TimelineMax();
  
  imgLoad.on( 'progress', function( instance, image ) {
      loadProgress();
  });
  
  function loadProgress(imgLoad, image) {

      loadedCount++;
    
      loadingProgress = Math.round(loadedCount/images);

      TweenMax.to(tlProgress, 1, {progress:loadingProgress});
  }

  var tlProgress = new TimelineMax({
      paused: true,
      onComplete: loadComplete
  });
  
  tlProgress
      .to(progressBar, 1, {width:"100%"});
  
  function loadComplete() {
    var tlEnd = new TimelineMax();
    tlEnd.to("#loading", 0.5, {opacity:0 ,display: 'none'});

    var indexAnimation = new TimelineMax();
    indexAnimation
        .fromTo(".dust-content", .8, {opacity:0 ,left: '0'},{opacity:1 ,ease:Quart.ease})
        .fromTo(".dyson", .8, {opacity:0 ,left: '100%'},{opacity:1 ,left: '0%' ,ease:Quart.ease})
        .fromTo(".dyson,.dust-content", .8, {left: '0%'},{left: '50%' ,ease:Linear.easeNone})
        .to(".kv-event", .8,{opacity: 1 ,ease:Linear.easeNone} ,'-=.8')
    // start all the timers
    $(".timer").each(count);
  }


  const swiper = new Swiper('.swiper', {
    loop: true,
    slidesPerView: 1.2,
    centeredSlides: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 1.5,
        centeredSlides: true
    },
      1200: {
          slidesPerView: 1.5,
          centeredSlides: true
      },
      2350: {
        slidesPerView: 2,
        centeredSlides: true
    }
    }
  });


});